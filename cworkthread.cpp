﻿#include "cworkthread.h"
#include <QMessageBox>
#include <QThread>
#include <QProcess>
#include <math.h>
#include <QPainter>

int g_step = 1;

CWorkThread::CWorkThread(QDialog *parent)
{
    m_isRun = false;
}

void CWorkThread::start()
{
    m_isRun = true;
    QThread::start();
}

void CWorkThread::stop()
{
    m_isRun = false;
}

void CWorkThread::run()
{
    int g_step = 1;

    while(m_isRun){
        QProcess pro;
        //截屏
        pro.execute("adb shell screencap -p /sdcard/autojump.png");
        QThread::msleep (600+rand()%10);
        //获取到PC
        pro.execute("adb pull /sdcard/autojump.png autojump.png");
        if(!oneStep()){
            return;
        }
        ++g_step;
    }
}

int CWorkThread::getPressTime(double iDistance){
    if(iDistance<200){
        iDistance = 200;
    }
    return iDistance*1.45+rand()%10;
}

bool CWorkThread::oneStep()
{
    int destX = 0;
    int destY = 0;
    int startX = 0;
    int startY = 0;
    //基准点
    unsigned char br=0,bg =0,bb = 0;
    QImage* img = new QImage;
    //加载PC截取到的图像
    if(img->load("autojump.png")){
#ifdef DEBUG
        QPainter painter(img);
#endif
        int width=img->width();//图像宽
        int height=img->height();//图像高
        unsigned char *data=img->bits();//获取图像像素字节数据的首地址
        unsigned char r,g,b;
        //从1/4处开始，跳过前面的像素点
        data += height/4*width*4-4;
        for (int i=height/4;i<height;i++)
        {
            for (int j=0;j<width;j++)
            {
                //destX == 0表示还没有获取到目的点
                if(destX == 0&& j==0){
                    //获取参照点
                    br = *(data+2);
                    bg = *(data+1);
                    bb = *data;
                }
                r = *(data+2);
                g = *(data+1);
                b = *data;
                data+=4;
                //br!=0 表示已获取参照点
                if(destX == 0&&br!=0){
                    //与参照点有较大差异，设为目的点
                    if(abs(r-br)>20||abs(g-bg)>20||abs(b-bb)>20){
                        //找到顶点，记录x,y
                        destX = j;
                        destY = i;
#ifdef DEBUG
                        painter.drawRect(j-10,i-10,20,20);
#endif
                        br = 92;
                        bg = 82;
                        bb = 133;
                    }
                }else{
                    //获取小人的位置
                    if(abs(r-br)<5&&abs(g-bg)<5&&abs(b-bb)<5){
                        startX = j;
                        startY = i;
                        //计算距离
                        int distance = sqrt(pow(startX-destX, 2) + pow(startY+40-destY, 2));
                        double ms  = getPressTime(distance);
#ifdef DEBUG
                        painter.drawRect(j-10,i-10,20,20);
                        painter.drawText(QRect(0,0,200,80),QString("距离：%1,按键时间:%2").arg(distance).arg(ms));
                        painter.end();
                        //如遇失败，可根据此图的方框是否获取正确判断识别的算法问题
                        img->save(QString("%1.png").arg(g_step));
#endif
                        //模拟按键，加入随机数，防止系统识别为机器作弊
                        QProcess pro;
                        pro.execute(QString("adb shell input swipe %1 %2 %1 %2 "+QString::number(ms)).arg(300+rand()%10).arg(400+rand()%16).toStdString().c_str());

                        QThread::msleep (ms+rand()%200+1000);
                        delete img;
                        return true;
                    }
                }
            }
        }
    }else{
        QMessageBox::about(NULL,"msg",QString("读取失败"));
    }
#ifdef DEBUG
    img->save(QString("%1.png").arg(g_step));
#endif
    delete img;
    return false;
}


