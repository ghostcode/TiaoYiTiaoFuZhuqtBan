﻿#include "cheatdlg.h"
#include "ui_cheatdlg.h"
#include <QMessageBox>
#include <QThread>
#include <QProcess>
#include <math.h>
#include <QPainter>
#include <QMessageBox>


CheatDlg::CheatDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CheatDlg)
{
    ui->setupUi(this);
    srand(1024);

    m_workThread = new CWorkThread(this);
}

CheatDlg::~CheatDlg()
{
    delete ui;
}

void CheatDlg::on_startbtn_clicked()
{
    QMessageBox::information(NULL, QString::fromLocal8Bit("友情提示"), QString::fromLocal8Bit("开始运行……"), QMessageBox::NoIcon);
    m_workThread->start();
}
void CheatDlg::on_stopbtn_2_clicked()
{
    m_workThread->stop();
    QMessageBox::information(NULL, QString::fromLocal8Bit("友情提示"), QString::fromLocal8Bit("停止运行"), QMessageBox::NoIcon);
}

